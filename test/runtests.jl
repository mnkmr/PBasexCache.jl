using PBasexCache
using Test

@testset "PBasexCache.jl" begin
    cachedir = mktempdir()
    PBasexCache.CACHEDIR[] = cachedir

    pbasex = PBasex(100)
    filename = joinpath(cachedir, "basisset.h5")
    savepbasex(filename, pbasex)

    @test loadpbasex(cachedir, 100) isa PBasex
    @test loadpbasex(100) isa PBasex
    pbx = loadpbasex(200)
    @test pbx.len == 100
end
