# PBasexCache

[![Build Status](https://gitlab.com/mnkmr/PBasexCache.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/PBasexCache.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/PBasexCache.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/PBasexCache.jl/commits/master)


## Installation

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add PBasexCache
```


## Usage

This package provides cached basis sets (len = 400, 600, and 800) of [`PBasexInversion.jl`](https://gitlab.com/mnkmr/PBasexCache.jl) and exports all the same functions `PBasexInversion.jl` does.

```
using PBasexCache
pbasex = PBasex(400)
inv = pbasex(img, (200.5, 200.5))
```
