module PBasexCache

using Reexport: @reexport
@reexport using PBasexInversion

const CACHEDIR = Ref{String}("")
function __init__()
    CACHEDIR[] = joinpath(@__DIR__, "..", "cache")
    nothing
end

"""
    loadpbasex(dir::AbstractString, limitlen::Int;
               match::Function=(f -> last(f, 3) == ".h5"),
               l=[0, 2], sigma::Real=2.0, σ::Real=sigma,
               rstep::Real=sqrt(σ/2), astep::Real=2.0,
               immediate=false)
    loadpbasex(limitlen::Int; kwargs...)

Search and load a reconstructor. A cache file is searched in the path `dir`,
which has the largest length not larger than `limitlen`. `l`, `σ`, `rstep`, and
`astep` are also checked whether these are matched or not if specified.

User can assign a function to the keyword argument `match` to filter files by
those names. It should accept an argument, file path, and return a boolean;
true if it should be a cache file, otherwise return false.

If `immediate` is true, the first cache file found is immediately returned.
"""
function PBasexInversion.loadpbasex(dir::AbstractString, limitlen::Int;
                                    match::Function=(f -> last(f, 3) == ".h5"),
                                    l=nothing,
                                    sigma::Union{Nothing,Integer}=nothing, σ=sigma,
                                    rstep::Union{Nothing,Real}=nothing,
                                    astep::Union{Nothing,Real}=nothing,
                                    immediate::Bool=false)
    list = PBasex[]
    for filename in sort(readdir(dir))
        last(filename, 3) != ".h5" && continue
        path = joinpath(dir, filename)
        !isfile(path) && continue
        pbx = loadpbasex(path)
        !ismatched(pbx, limitlen, l, σ, rstep, astep) && continue
        immediate && return pbx
        push!(list, pbx)
    end
    return largest(list)
end
PBasexInversion.loadpbasex(limitlen::Int; kwargs...) = loadpbasex(CACHEDIR[], limitlen; kwargs...)


function ismatched(pbx::PBasex, limitlen::Integer, l, σ, rstep, astep)
    pbx.len > limitlen && return false
    !isnothing(rstep) && pbx.rstep != rstep && return false
    !isnothing(astep) && pbx.astep != astep && return false
    !isnothing(l) && any(sort(pbx.l) .!= sort(l)) && return false
    !isnothing(σ) && pbx.σ != σ && return false
    return true
end


function largest(list::Array{PBasex,1})
    if isempty(list)
        return nothing
    end
    sort(list, by = x -> x.len)
    last(list)
end


end # module
