using PBasexInversion

if haskey(ENV, "PBASEXCACHE_JL_SIZES")
    lengths = map(s -> parse(Int, s), split(ENV["PBASEXCACHE_JL_SIZES"], ","))
else
    lengths = [400, 600, 800]
end

for len in lengths
    filename = normpath(joinpath(@__DIR__, "..", "cache", "basisset_$len.h5"))
    if isfile(filename)
        fileinfo = statpbasex(filename)
        if fileinfo.__VERSION__ == PBasexInversion.PBASEX_FILE_VERSION && fileinfo.len == len
            println("\"$filename\" already exists. Skipped.")
            continue
        end
    end
    println("Building a p-Basex reconstructor of $len×$len...")
    pbasex = PBasex(len, show_progress=false)
    savepbasex(filename, pbasex)
    println("Saved as $filename")
end
